import { CallHandler, ExecutionContext, ForbiddenException, NestInterceptor } from "@nestjs/common";
import { Observable } from "rxjs";
import { Request } from "express";

import { Logger } from "../lib";

import { AuthHelper } from "@app/entity-service/modules/authetication";

// export function CheckRoleFuncMiddleware(
//     role: string,
// ): (req: any, res: any, next: (err?: any) => Promise<any>) => Promise<any> {
//     return (req: any, res: any, next: (err?: any) => Promise<any>) => {
//         const log = new Logger(__filename);
//
//         try {
//             const token = req.headers.authorization.split(" ", 2)[1];
//             const payload = AuthHelper.getPayloadFromJWT(token);
//
//             if (!payload.kind || payload.kind !== role) {
//                 throw new Error("Permission denied");
//             }
//
//             return next();
//         } catch (error: any) {
//             log.error(error.stack);
//             throw new HttpError(403, "Permission denied");
//         }
//     };
// }

export function CheckRoleFuncMiddleware(role) {
    return class implements NestInterceptor {
        intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
            const log = new Logger(__filename);

            try {
                const token = context.switchToHttp().getRequest<Request>().headers.authorization?.split(" ", 2)[1];
                const payload = AuthHelper.getPayloadFromJWT(token);

                if (!payload.kind || payload.kind !== role) {
                    throw new Error("Permission denied");
                }

                return next.handle();
            } catch (error: any) {
                log.error(error.stack);
                throw new ForbiddenException();
            }
        }
    };
}

export const UserRoles = {
    admin: "admin",
};
