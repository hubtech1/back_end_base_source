import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { map, Observable } from "rxjs";
import { Request } from "express";
import { plainToClass } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

import { FullQueryDto } from "@app/common/dto/query.dto";

export class PaginationMeta {
    @ApiProperty()
    current: number;
    @ApiProperty()
    next: number;
    @ApiProperty()
    prev: number;
    @ApiProperty()
    take: number;
    @ApiProperty()
    total: number;
}

export class PaginateJsonResponse<T> {
    @ApiProperty()
    code: number;
    @ApiProperty()
    data: T[];
    @ApiProperty({ type: PaginationMeta })
    pagination: PaginationMeta;
    [key: string]: any;
}

@Injectable()
export class ListResponseInterceptor<T = any> implements NestInterceptor<T> {
    public intercept(context: ExecutionContext, next: CallHandler): Observable<PaginateJsonResponse<T>> {
        const request = context.switchToHttp().getRequest<Request>();
        return next.handle().pipe(
            map((content: any) => {
                if (content === undefined || content === null || content === []) {
                    return undefined;
                }

                const option = plainToClass(FullQueryDto, request.query);

                let page = 1;

                if (option.page && option.take) {
                    page = +option.page;
                } else if (option.skip && option.take) {
                    page = Math.floor(+option.skip / +option.take) + 1;
                }

                return {
                    ...content,
                    data: content?.list,
                    pagination: {
                        current: page,
                        next: page + 1,
                        prev: page - 1,
                        take: option.take,
                        total: content?.count || 0,
                    },
                };
            }),
        );
    }
}
