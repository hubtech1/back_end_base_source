import { Repository, SelectQueryBuilder } from "typeorm";
import { isEmpty } from "class-validator";
import { normalizeSync } from "normalize-diacritics";
import { BadRequestException } from "@nestjs/common";
import * as uuid from "uuid";

import { CommonUtil } from "./index";

export class ConditionQueryBuilder<T extends any> {
    public static ofRepository<T extends any>(repository: Repository<T>): ConditionQueryBuilder<T> {
        return new ConditionQueryBuilder<T>(repository);
    }
    private _queryBuilder: SelectQueryBuilder<T>;
    private _isRelationAdded = false;
    private readonly _target: any;
    private readonly _leftJoins: string[] = [];
    private readonly _leftJoinAndSelects: string[] = [];

    private constructor(private readonly repository: Repository<T>) {
        this._queryBuilder = this.repository.createQueryBuilder("this");
        this._target = this.repository.target;
    }

    private findAliasAndRelatedName(params: {
        oldAliases: object;
        seqRelations: object;
        alias: string;
        relation: string;
        key: string;
    }): { newAlias: string; relatedName: string } {
        const { oldAliases, seqRelations, alias, relation, key } = params;

        let newAlias = alias;
        if (seqRelations[newAlias] > 0) {
            const count = seqRelations[newAlias];
            newAlias = `${newAlias}_${count}`;
            oldAliases[relation] = newAlias;
        } else {
            oldAliases[relation] = newAlias;
        }

        const oldAlias = oldAliases[key];
        const relatedName = `${oldAlias}.${alias}`;

        return { newAlias, relatedName };
    }

    private findKeyAndAlias(relation: string): { key: string; alias: string } {
        const [relate, relateLv1, relateLv2, relateLv3, relateLv4] = relation.split(".");

        if (relateLv4) {
            const key = `${relate}.${relateLv1}.${relateLv2}.${relateLv3}`;
            return { key, alias: relateLv4 };
        } else if (relateLv3) {
            const key = `${relate}.${relateLv1}.${relateLv2}`;
            return { key, alias: relateLv3 };
        } else if (relateLv2) {
            const key = `${relate}.${relateLv1}`;
            return { key, alias: relateLv2 };
        } else if (relateLv1) {
            const key = relate;
            return { key, alias: relateLv1 };
        }
    }

    public relations(relations: string[] = []): this {
        const seqRelations = {};
        const oldAliases = {};

        for (const relation of relations) {
            const [relate, relateLv1, relateLv2, relateLv3, relateLv4] = relation.split(".");
            let newAlias: string;
            let relatedName: string;

            if (relateLv4 || relateLv3 || relateLv2 || relateLv1) {
                const { key, alias } = this.findKeyAndAlias(relation);
                const result = this.findAliasAndRelatedName({
                    oldAliases,
                    seqRelations,
                    alias,
                    relation,
                    key,
                });

                relatedName = result.relatedName;
                newAlias = result.newAlias;
            } else {
                newAlias = relate;
                oldAliases[relation] = newAlias;

                relatedName = `this.${relate}`;
            }

            if (!seqRelations[newAlias]) {
                seqRelations[newAlias] = 1;
            } else {
                seqRelations[newAlias]++;
            }

            try {
                if (!this._leftJoinAndSelects.includes(newAlias)) {
                    this._queryBuilder = this._queryBuilder.leftJoinAndSelect(relatedName, newAlias);
                    this._leftJoinAndSelects.push(newAlias);
                }
            } catch (e: any) {
                throw new BadRequestException(`Relations error: ${e.message || e}`);
            }
        }
        this._isRelationAdded = true;
        return this;
    }

    public select(selection?: string[]): this {
        if (!selection || !Array.isArray(selection)) {
            return this;
        }
        const columns = [];

        for (const select of selection) {
            // Column in table
            if (!select.includes(".")) {
                columns.push(select);
            }
        }

        this._queryBuilder = this._queryBuilder.select(columns.map((col) => `this.${col}`));

        return this;
    }

    public addSelect(selection?: string[]): this {
        if (!selection || !Array.isArray(selection)) {
            return this;
        }
        const columns = [];

        for (const select of selection) {
            // Column in relation table
            if (select.includes(".")) {
                columns.push(select);
            }
        }

        this._queryBuilder = this._queryBuilder.addSelect(columns);

        return this;
    }

    public condition(conditionMap?: Record<string, ICondition>, parentField = "this"): this {
        this._checkRelation();
        const seqCondition = {
            eq: 0,
            lt: 0,
            gt: 0,
            ge: 0,
            le: 0,
            not: 0,
            in: 0,
            nin: 0,
            fs: 0,
            s: 0,
        };

        if (!conditionMap || isEmpty(conditionMap)) {
            return this;
        }

        for (const field of Object.keys(conditionMap)) {
            const condition = conditionMap[field];
            if (condition["fs"]) {
                this.parseCondition(`${parentField}.fullTextSearch`, condition, seqCondition);
            } else {
                this.parseCondition(`${parentField}.${field}`, condition, seqCondition);
            }
        }
        return this;
    }

    public one(id: string): this {
        if (id === undefined || id === "" || id === null) {
            return this;
        }

        if (uuid.validate(id)) {
            this._queryBuilder = this._queryBuilder.where("this.id = :id", { id });
        } else if (CommonUtil.checkShortID(id)) {
            this._queryBuilder = this._queryBuilder.where("this.shortID = :id", { id });
        } else if (CommonUtil.checkUrl(id)) {
            this._queryBuilder = this._queryBuilder.where("this.domain = :id", { id });
        } else {
            this._queryBuilder = this._queryBuilder.where("this.slug = :id", { id });
        }

        return this;
    }

    public take(take: number): this {
        this._queryBuilder = this._queryBuilder.take(take);
        return this;
    }

    public skip(skip: number): this {
        this._queryBuilder = this._queryBuilder.skip(skip);
        return this;
    }

    public offset(areYouSure: boolean, offset = 0): this {
        if (!areYouSure) {
            throw new Error("You don't sure to use this");
        }

        this._queryBuilder = this._queryBuilder.offset(offset);
        return this;
    }

    public order(orders?: Record<string, number>): this {
        if (!orders) {
            return this;
        }

        for (const orderField of Object.keys(orders)) {
            const field = orderField.includes(".") ? orderField : `this.${orderField}`;
            const orderDir = orders[orderField] > 0 ? "ASC" : "DESC";

            this._queryBuilder = this._queryBuilder.addOrderBy(field, orderDir, "NULLS LAST");
        }
        return this;
    }

    public search(search?: string): this {
        if (!!search) {
            const searchNormalize = normalizeSync(search.trim());
            this._queryBuilder = this._queryBuilder.andWhere(`this.fullTextSearch ILIKE :search`, {
                search: `%${searchNormalize}%`,
            });
        }
        return this;
    }

    public build(): SelectQueryBuilder<T> {
        return this._queryBuilder;
    }

    private hasRelation(field: string): boolean {
        const [relation] = field.split(".");
        return this._queryBuilder.hasRelation(this._target, relation);
    }

    private parseCondition(field: string, conditions: ICondition, seqCondition: object): this {
        if (typeof conditions === "number" || typeof conditions === "string") {
            const param = `${field}`.toLowerCase().replace(".", "__");

            this._queryBuilder = this._queryBuilder.andWhere(`${field} = :${param}`, { [param]: conditions });
            return this;
        }

        const keys = Object.keys(conditions) as IConditionType[];
        for (const key of keys) {
            const value = conditions[key];
            const parameter = {};
            let count = 0;

            switch (key) {
                case IConditionType.eq:
                    count = seqCondition[IConditionType.eq];
                    seqCondition[IConditionType.eq]++;
                    parameter[`${IConditionType.eq}_${count}`] = value;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} = :${IConditionType.eq}_${count}`,
                        parameter,
                    );
                    continue;
                case IConditionType.ge:
                    count = seqCondition[IConditionType.ge];
                    seqCondition[IConditionType.ge]++;
                    parameter[`${IConditionType.ge}_${count}`] = value;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} >= :${IConditionType.ge}_${count}`,
                        parameter,
                    );
                    continue;
                case IConditionType.gt:
                    count = seqCondition[IConditionType.gt];
                    seqCondition[IConditionType.gt]++;
                    parameter[`${IConditionType.gt}_${count}`] = value;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} > :${IConditionType.gt}_${count}`,
                        parameter,
                    );
                    continue;
                case IConditionType.le:
                    count = seqCondition[IConditionType.le];
                    seqCondition[IConditionType.le]++;
                    parameter[`${IConditionType.le}_${count}`] = value;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} <= :${IConditionType.le}_${count}`,
                        parameter,
                    );
                    continue;
                case IConditionType.lt:
                    count = seqCondition[IConditionType.lt];
                    seqCondition[IConditionType.lt]++;
                    parameter[`${IConditionType.lt}_${count}`] = value;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} < :${IConditionType.lt}_${count}`,
                        parameter,
                    );
                    continue;
                case IConditionType.not:
                    count = seqCondition[IConditionType.not];
                    seqCondition[IConditionType.not]++;
                    parameter[`${IConditionType.not}_${count}`] = value;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} != :${IConditionType.not}_${count}`,
                        parameter,
                    );
                    continue;
                case IConditionType.in:
                    count = seqCondition[IConditionType.in];
                    seqCondition[IConditionType.in]++;
                    parameter[`${IConditionType.in}_${count}`] = value;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} IN (:...${IConditionType.in}_${count})`,
                        parameter,
                    );
                    continue;
                case IConditionType.nin:
                    count = seqCondition[IConditionType.nin];
                    seqCondition[IConditionType.nin]++;
                    parameter[`${IConditionType.nin}_${count}`] = value;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} NOT IN (:...${IConditionType.nin}_${count})`,
                        parameter,
                    );
                    continue;
                case IConditionType.s:
                    count = seqCondition[IConditionType.s];
                    seqCondition[IConditionType.s]++;
                    parameter[`${IConditionType.s}_${count}`] = `%${value}%`;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} ILIKE :${IConditionType.s}_${count}`,
                        parameter,
                    );
                    continue;
                case IConditionType.fs:
                    const searchNormalize = normalizeSync(value.trim());
                    count = seqCondition[IConditionType.fs];
                    seqCondition[IConditionType.fs]++;
                    parameter[`${IConditionType.fs}_${count}`] = `%${searchNormalize}%`;

                    this._queryBuilder = this._queryBuilder.andWhere(
                        `${field} ILIKE :${IConditionType.fs}_${count}`,
                        parameter,
                    );
                    continue;
                case IConditionType.inull:
                    this._queryBuilder = this._queryBuilder.andWhere(`${field} IS NULL`);
                    continue;
                case IConditionType.innull:
                    this._queryBuilder = this._queryBuilder.andWhere(`${field} IS NOT NULL`);
                    continue;
                default: {
                    const alias = `${field}`.replace(".", "_");
                    if (!this._leftJoins.includes(alias) && !this._leftJoinAndSelects.includes(alias)) {
                        this._queryBuilder = this._queryBuilder.leftJoin(field, alias);
                        this._leftJoins.push(alias);
                    }
                    return this.condition(conditions as Record<string, ICondition>, alias);
                }
            }
        }
        return this;
    }

    private _checkRelation(): void {
        if (!this._isRelationAdded) {
            throw new Error("function `relation` must be called before `condition`");
        }
    }
}

export enum IConditionType {
    eq = "eq", // equal
    lt = "lt", // less than
    gt = "gt", // greater than
    ge = "ge", // greater or equal
    le = "le", // less or equal
    not = "not", // not equal
    in = "in", // in array
    nin = "nin", // not in array
    fs = "fs", // full text search
    s = "s", // normal search
    inull = "inull", // is null
    innull = "innull", // is not null,
}

export type ICondition =
    | {
          [key in IConditionType]?: string | number;
      }
    | {
          [key: string]: any;
      };
