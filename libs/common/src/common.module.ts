import { Module, Scope } from "@nestjs/common";

import { Logger } from "./lib";
import { DBUtil, ParseUtil, CommonUtil } from "./utils";
import {
    CalculateService,
    IdentityService,
    PushLogService,
    GenerateImageService,
} from "./services";

const SERVICES = [
    CalculateService,
    IdentityService,
    PushLogService,
    GenerateImageService,
    Logger,
    DBUtil,
    ParseUtil,
    CommonUtil,
];

@Module({
    providers: [
        {
            provide: Logger,
            useFactory: () => new Logger(),
            scope: Scope.TRANSIENT,
        },
        ...SERVICES,
    ],
    exports: [...SERVICES],
})
export class CommonModule {}
