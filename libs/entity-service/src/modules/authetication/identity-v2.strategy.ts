import { PassportStrategy } from "@nestjs/passport";
import { Strategy, VerifiedCallback } from "passport-custom";
import { HttpException, Injectable, UnauthorizedException } from "@nestjs/common";
import { Request } from "express";
import axios from "axios";

import { AuthHelper } from "./auth.helper";

import { Logger, env } from "@app/common";

@Injectable()
export class IdentityV2Strategy extends PassportStrategy(Strategy, "identity-v2") {
    private logger = new Logger(__filename);
    private roles = [];

    async validate(request: Request, done: VerifiedCallback) {
        const isDebugHeader = request.headers["is-debug"];

        // ignore check token for quick debug
        if (env.node === "development" && isDebugHeader) {
            return done(null, {});
        }

        // check access token in authorization
        if (!request.headers.authorization) {
            throw new UnauthorizedException();
        }

        try {
            // @ts-ignore
            const controller = request.url.replace(env.app.routePrefix + "/", "").split("/", 2)[0];

            const [, token] = request.headers.authorization.split(" ", 2);
            const jwtPayload = AuthHelper.getPayloadFromJWT(token);

            if (!controller || !token) {
                return done(true);
            }

            const [type] = this.roles;

            if (type === "verify-permission") {
                const method = this.getActionFromMethod(request.method);
                const permission = `entity.${jwtPayload.farmID}.${controller}.${method}`;

                this.logger.info(
                    `make GET ${env.hubtech.identityServiceV2}users/verify-permission?action=${permission}`,
                );
                await axios.create({ baseURL: env.hubtech.identityServiceV2 })({
                    url: "/users/verify-permission",
                    headers: { authorization: `Bearer ${token}` },
                    method: "GET",
                    params: { action: permission },
                });
            } else {
                this.logger.info(`make GET ${env.hubtech.identityServiceV2}users/verify-token`);
                await axios.create({ baseURL: env.hubtech.identityServiceV2 })({
                    url: "/users/verify-token",
                    headers: { authorization: `Bearer ${token}` },
                    method: "GET",
                });
            }

            return done(null, jwtPayload);
        } catch (resError: any) {
            if (resError.response) {
                this.logger.error(
                    `[Identity Service V2 response] ${resError.response.status} ${resError.response.data.message}`,
                );
                throw new HttpException(resError.response.data.message, resError.response.status);
            } else {
                this.logger.error(resError.stack);
                return done(resError);
            }
        }
    }

    getActionFromMethod(method: string): string {
        switch (method.toLowerCase()) {
            case "get":
                return "get";
            case "post":
                return "create";
            case "put":
            case "path":
                return "update";
            case "delete":
                return "delete";
            default:
                return "unknown";
        }
    }
}
