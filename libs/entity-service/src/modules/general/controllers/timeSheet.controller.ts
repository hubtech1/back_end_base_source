import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, UseInterceptors } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";


import { TimeSheetRepository } from "../repositories";
import { TimeSheetNotFoundError } from "../errors";
import { TimeSheetService } from "../services";

import {
    ParseUtil,
    ListDataDto,
    BaseQueryDto,
    FullQueryDto,
    ErrorResponseDto,
    ListResponseInterceptor,
    OnUndefinedInterceptor,
    WhereQueryDto,
} from "@app/common";
import { CreateTimeSheetBodyDto, TimeSheetResponseDto, UpdateTimeSheetBodyDto } from "../dto";
import { TimeSheet } from "../entities";
import { Authorized } from "type-graphql";
import { IndexMetadata } from "typeorm/metadata/IndexMetadata";


@ApiTags("timesheets")
@Controller("/timesheets")
@ApiResponse({ type: ErrorResponseDto, status: 401 })
@ApiResponse({ type: ErrorResponseDto, status: 404 })
@ApiResponse({ type: ErrorResponseDto, status: 500 })
export class TimeSheetController {
    constructor(
        private timeSheetService: TimeSheetService,
        private parseUtil: ParseUtil,
        private timeSheetRepository: TimeSheetRepository,

    ) { }

    // Lấy hết danh sách Timesheet
    @Get()
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated', 
        `
    })
    @UseInterceptors(ListResponseInterceptor)
    @OnUndefinedInterceptor(TimeSheetNotFoundError)
    @ApiResponse({ type: TimeSheetResponseDto, isArray: true, status: 200 })
    public find(
        @Query() query: FullQueryDto,

    ): Promise<ListDataDto<TimeSheet> | undefined> {
        const queryParse = this.parseUtil.fullQueryParam(query)
        return this.timeSheetService.find(queryParse)
    }

    // Tạo thêm Timesheet
    // @Authorized()
    @Post()
    @ApiOperation({
        description: `
        role: 0: 'staff', 1: 'manager', 2: 'director'`,
    })
    @OnUndefinedInterceptor(TimeSheetNotFoundError)
    @ApiResponse({ type: TimeSheetResponseDto, status: 200 })
    public async create(
        @Body() body: CreateTimeSheetBodyDto,
    ): Promise<TimeSheet | undefined> {
        return this.timeSheetService.create(body)
    }

    // Tìm 1 Timesheer theo id
    @Get("/:id")
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated',
        role: 0: 'staff', 1: 'manager', 2: 'director', 
        `
    })
    @OnUndefinedInterceptor(TimeSheetNotFoundError)
    @ApiResponse({ type: TimeSheetResponseDto, status: 200 })
    public async findOne(
        @Param("id") id: string,
        @Query() query: BaseQueryDto,
    ): Promise<TimeSheet | undefined> {
        const queryParse = this.parseUtil.baseQueryParam(query);
        return this.timeSheetService.findOne(id, queryParse)
    }

    // Update thông tin Timesheet

    @Authorized()
    @Patch("/:id")
    @OnUndefinedInterceptor(TimeSheetNotFoundError)
    @ApiResponse({ type: TimeSheetResponseDto, status: 200 })
    public async update(
        @Param("id") id: string,
        @Body() body: UpdateTimeSheetBodyDto
    ): Promise<TimeSheet | undefined> {
        const bodyParse = this.parseUtil.removeUndefinedProperty(body);
        return this.timeSheetService.update(id, bodyParse)
    }

    // Xóa Timesheet
    @Authorized()
    @Delete("/:id")
    @OnUndefinedInterceptor(TimeSheetNotFoundError)
    public async delete(
        @Param("id") id: string,
    ): Promise<TimeSheet | undefined> {
        return this.timeSheetService.delete(id)
    }













}


