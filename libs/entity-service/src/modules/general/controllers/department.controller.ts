import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, UseInterceptors } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";


import { DepartmentRepository } from "../repositories";
import { DepartmentNotFoundError } from "../errors";
import { DepartmentService } from "../services";

import {
    ParseUtil,
    ListDataDto,
    BaseQueryDto,
    FullQueryDto,
    ErrorResponseDto,
    ListResponseInterceptor,
    OnUndefinedInterceptor,
    WhereQueryDto,
} from "@app/common";
import { CreateDepartmentBodyDto, DepartmentResponseDto, UpdateDepartmentBodyDto } from "../dto";
import { Department } from "../entities";
import { Authorized } from "type-graphql";
import { IndexMetadata } from "typeorm/metadata/IndexMetadata";


@ApiTags("departments")
@Controller("/departments")
@ApiResponse({ type: ErrorResponseDto, status: 401 })
@ApiResponse({ type: ErrorResponseDto, status: 404 })
@ApiResponse({ type: ErrorResponseDto, status: 500 })
export class DepartmentController {
    constructor(
        private departmentService: DepartmentService,
        private parseUtil: ParseUtil,
        private departmentRepository: DepartmentRepository,

    ) { }

    // Lấy hết danh sách Department
    @Get()
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated', 
        `
    })
    @UseInterceptors(ListResponseInterceptor)
    @OnUndefinedInterceptor(DepartmentNotFoundError)
    @ApiResponse({ type: DepartmentResponseDto, isArray: true, status: 200 })
    public find(
        @Query() query: FullQueryDto,

    ): Promise<ListDataDto<Department> | undefined> {
        const queryParse = this.parseUtil.fullQueryParam(query)
        return this.departmentService.find(queryParse)
    }

    // Tạo thêm Department
    // @Authorized()
    @Post()
    @ApiOperation({
        description: `
        role: 0: 'staff', 1: 'manager', 2: 'director'`,
    })
    @OnUndefinedInterceptor(DepartmentNotFoundError)
    @ApiResponse({ type: DepartmentResponseDto, status: 200 })
    public async create(
        @Body() body: CreateDepartmentBodyDto,
    ): Promise<Department | undefined> {
        return this.departmentService.create(body)
    }

    // Tìm 1 Department theo id
    @Get("/:id")
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated',
        role: 0: 'staff', 1: 'manager', 2: 'director', 
        `
    })
    @OnUndefinedInterceptor(DepartmentNotFoundError)
    @ApiResponse({ type: DepartmentResponseDto, status: 200 })
    public async findOne(
        @Param("id") id: string,
        @Query() query: BaseQueryDto,
    ): Promise<Department | undefined> {
        const queryParse = this.parseUtil.baseQueryParam(query);
        return this.departmentService.findOne(id, queryParse)
    }

    // Update thông tin Department

    @Authorized()
    @Patch("/:id")
    @OnUndefinedInterceptor(DepartmentNotFoundError)
    @ApiResponse({ type: DepartmentResponseDto, status: 200 })
    public async update(
        @Param("id") id: string,
        @Body() body: UpdateDepartmentBodyDto
    ): Promise<Department | undefined> {
        const bodyParse = this.parseUtil.removeUndefinedProperty(body);
        return this.departmentService.update(id, bodyParse)
    }

    // Xóa Department
    @Authorized()
    @Delete("/:id")
    @OnUndefinedInterceptor(DepartmentNotFoundError)
    public async delete(
        @Param("id") id: string,
    ): Promise<Department | undefined> {
        return this.departmentService.delete(id)
    }













}