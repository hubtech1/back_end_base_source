import { HttpException } from "@nestjs/common";


export class StaffNotFoundError extends HttpException {
    constructor() {
        super("Staff not found!", 404)
    }
}