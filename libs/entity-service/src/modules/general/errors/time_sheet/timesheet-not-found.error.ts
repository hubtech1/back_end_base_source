import { HttpException } from "@nestjs/common";


export class TimeSheetNotFoundError extends HttpException {
    constructor() {
        super("Time sheet not found!", 404)
    }
}