import _ from "lodash";
import * as uuid from "uuid";
import { Injectable } from "@nestjs/common";

import { DepartmentRepository } from "../repositories";

import { DBUtil, Logger, ListDataDto, CrudOption, env, CommonUtil } from "@app/common";
import { CreateDepartmentBodyDto, UpdateDepartmentBodyDto } from "@app/entity-service/modules/general/dto";
import { Department } from "../entities";


@Injectable()
export class DepartmentService {
    constructor(
        private departmentRepository: DepartmentRepository,
        private dbUtil: DBUtil,
        private commonUtil: CommonUtil,
        private logger: Logger
    ) { }

    public async find(option: CrudOption = {}): Promise<ListDataDto<Department> | undefined> {
        this.logger.info("Find all Department");
        return this.dbUtil.findAndCount(this.departmentRepository, option);
    }

    public async create(
        body: CreateDepartmentBodyDto,
        option: CrudOption = {},
    ): Promise<Department> {
        this.logger.info("Create a new Department");

        const department = new Department;
        _.assign(department, body);
        department.id = uuid.v1()

        return this.departmentRepository.save(department)
    }

    public async findOne(id: string, option: CrudOption = {}): Promise<Department> {
        this.logger.info("Find one Department");

        const result = await this.dbUtil.findOne(this.departmentRepository, id, option);

        if (!result) {
            return result
        }

        return result
    }

    public async update(id: string, body: UpdateDepartmentBodyDto, option?: CrudOption): Promise<Department | undefined> {
        this.logger.info("Update some fields a Department");

        await this.departmentRepository.update(id, body);

        return this.departmentRepository.findOne(id)
    }

    public async delete(id: string, option?: CrudOption): Promise<Department | undefined> {
        this.logger.info("Delete a Department");
        const item = await this.departmentRepository.findOne(id);
        if (item === undefined) {
            return undefined
        } else {
            await this.departmentRepository.delete(id)
        }
        return item
    }
}