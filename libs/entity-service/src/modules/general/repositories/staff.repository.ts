import { EntityRepository, Repository } from "typeorm";
import { Staff } from "../entities";



@EntityRepository(Staff)
export class StaffRepository extends Repository<Staff> {

}