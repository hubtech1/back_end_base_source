import { EntityRepository, Repository } from "typeorm";
import { TimeSheet } from "../entities";



@EntityRepository(TimeSheet)
export class TimeSheetRepository extends Repository<TimeSheet> {

}