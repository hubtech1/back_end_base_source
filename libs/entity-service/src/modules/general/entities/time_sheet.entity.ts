import { Column, Entity as EntityTypeORM } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";

import { Base } from "@app/common/entities";

@EntityTypeORM()
@ObjectType()
export class TimeSheet extends Base {
    @Column({ name: "staff_id", nullable: false })
    @Field()
    public staffId: string;

    @Column({ name: "date_id", nullable: false })
    @Field()
    public dateId: string;

    @Column({ name: "start_time", nullable: false })
    @Field()
    public startTime: string;

    @Column({ name: "time_spent", nullable: false })
    @Field()
    public timeSpent: number;

    @Column({ name: "title", nullable: true })
    @Field()
    public title: string;

    @Column({ name: "description", nullable: true })
    @Field()
    public description: string;

    @Column({ name: "attach", nullable: true })
    @Field()
    public attach: string;

}