# Database Caching

---

> Tham khảo [https://github.com/typeorm/typeorm/blob/master/docs/caching.md](https://github.com/typeorm/typeorm/blob/master/docs/caching.md)

---

Sử dụng chức năng Caching có sẵn của [TypeORM](https://github.com/typeorm/typeorm)

Và setup để TypeORM kết nối đến Redis:

(file `src/loaders/typeormLoader.ts`)

```typescript
const connectionOptions = Object.assign(loadedConnectionOptions, {
    // ....
    // other options
    // ....
    cache: env.redis.caching
        ? {
              type: 'redis',
              options: {
                  url: env.redis.url,
              },
              ignoreErrors: true,
              duration: env.redis.cache_time,
          }
        : false,
} as ConnectionOptions);
```

Sample:

```typescript
// Repository pattern
const results = await CategoryController.find({
    where: {
        // ...
    },
    skip: {
        // ...
    },

    // caching
    cache: true,
    // or cache: 1000 if you want to set timeout = 1s
});
```

```typescript
// Query builder
const results = await createQueryBuilder(Category)
    .where('...')
    // ....
    .cache(true) // or .cache(1000)
    .find();
```
