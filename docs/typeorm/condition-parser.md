# Condition query builder

---

Condition Query Builder là tính năng phát triển thêm của Farm Service
dựa trên [Query Builder](/typeorm/custom-query.md) của [TypeORM](https://github.com/typeorm/typeorm)

Một số tính năng mà `TypeORM` chưa hỗ trợ là:

-   Câu điều kiện với toán tử `>`, `<`, `!=`

Ví dụ ta có câu điều kiện query từ request:

```json
{
    "where": {
        "area": {
            "farmID": "some farm id"
        },
        "price": { "gt": 10000 }
    },
    "select": ["id", "name"]
}
```

thì SQL sẽ là:

```sql
SELECT (...), `area`.`farmID` as `area_farmID` FROM [Table]
    WHERE `area_farmID` = "some farm id" AND `price` > 10000
    LEFT JOIN [area] ON (...)
```

Vì câu điều kiện có thể dẫn đến việc ta phải cấu trúc lại cái bảng để `join`
và thay đổi input điều kiện thành các keyword như `gt` -> `>` hay `lt` -> `<`

## Using Code

Example code [src/api/services/common/DbHelper.ts]()

Sử dụng:

```typescript
const repository = getRepository(Category);
const query = ConditionQueryBuilder.ofRepository(repository)
    .select(['...']) // required
    // ...
    .condition({
        // ...
    });

const results = await query.find();
```

## Cách chuyển đổi (parse)

> Tài liệu tham khảo:
>
> [https://github.com/typeorm/typeorm/blob/a868078f8f4f63cc8d81a63e098d639ed12c6608/src/query-builder/QueryBuilder.ts#L822](https://github.com/typeorm/typeorm/blob/a868078f8f4f63cc8d81a63e098d639ed12c6608/src/query-builder/QueryBuilder.ts#L822)
>
> [https://github.com/typeorm/typeorm/blob/master/src/query-builder/SelectQueryBuilder.ts#L672](https://github.com/typeorm/typeorm/blob/master/src/query-builder/SelectQueryBuilder.ts#L672)
>
> [https://github.com/typeorm/typeorm/blob/a868078f8f4f63cc8d81a63e098d639ed12c6608/src/query-builder/SelectQueryBuilder.ts#L148](https://github.com/typeorm/typeorm/blob/a868078f8f4f63cc8d81a63e098d639ed12c6608/src/query-builder/SelectQueryBuilder.ts#L148)

Ví dụ ta có input

```json
{
    "where": {
        "id": 123456,
        "price": { "gt": 1000 },
        "area": {
            "farmID": 123
        }
    }
}
```

Giả sử ta đang query bảng Product, ta tạo alias là `this`

Ta có 3 trường hợp trong object where:

-   TH 1: `key: value`: là quan hệ `=` cơ bản (`this.id = 123456`)

-   TH 2: `key: object`:

    -   TH 2.1: `object` bên trong có `gt`, `lt`, `eq`, `not`

        Trường hợp này ta build ra dạng `this.key [operater] value` (VD: `this.price > 1000`)

    -   TH 2.2: `object` khác với 2.1

        Check xem `key` lúc này phải relation hay không, nếu có:

        -   Join bảng `key` vào query với alias mới (VD: `join` bảng `area` vào và gán alias là `this_area`)
        -   Đệ quy về TH.1 (VD: `this_area.farmID = 123`)

        Nếu không, báo lỗi

-   TH 3: Báo lỗi

---

Implementation: [src/api/services/common/ConditionQueryBuilder.ts]()
