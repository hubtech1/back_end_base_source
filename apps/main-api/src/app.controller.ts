import { Controller, Get } from "@nestjs/common";
import { ApiResponse } from "@nestjs/swagger";

import { AppService } from "./app.service";

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) { }

    @Get()
    @ApiResponse({ type: Object })
    getHello() {
        return this.appService.getHello();
    }
}
