import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { env } from "@app/common";
import { Area } from "@app/entity-service/modules/general/entities";

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: env.db.type as any, // See createConnection options for valid types
            host: env.db.host,
            port: env.db.port,
            username: env.db.username,
            password: env.db.password,
            database: env.db.database,
            entities: [Area],
            synchronize: true,
            logging: env.db.logging as any,
            logger: env.db.logger as any,
            // entities: env.app.dirs.entities, // using autoload - no need this anymore
            // migrations: env.app.dirs.migrations,
            autoReconnect: true,
            autoLoadEntities: true,
            cache: env.redis.caching
                ? {
                    type: "redis",
                    options: {
                        url: env.redis.url,
                    },
                    ignoreErrors: true,
                    duration: env.redis.cache_time,
                }
                : false,
        }),
    ],
    exports: [TypeOrmModule],
})
export class TypeormLoader { }
